/*
Created		25. 11. 2018
Modified		26. 11. 2018
Project		
Model		
Company		
Author		
Version		
Database		mySQL 5 
*/


Create table User (
	User_ID Int NOT NULL AUTO_INCREMENT,
	Name Varchar(255) NOT NULL,
	Email Varchar(255) NOT NULL,
	Email_verified_at Datetime DEFAULT NULL,
	Password Varchar(255) NOT NULL,
	Remember_token Varchar(100),
	Google_id Varchar(255),
	Is_admin Bool NOT NULL DEFAULT False,
	Created Datetime NOT NULL,
	Updated Datetime NOT NULL,
 Primary Key (User_ID)) ENGINE = MyISAM;

Create table Role (
	Role_ID Int NOT NULL AUTO_INCREMENT,
	Name Varchar(255) NOT NULL,
	Description Text,
	Created Datetime NOT NULL,
	Updated Datetime NOT NULL,
 Primary Key (Role_ID)) ENGINE = MyISAM;

Create table Role_user (
	Role_user_ID Int NOT NULL AUTO_INCREMENT,
	User_ID Int NOT NULL,
	Created Datetime NOT NULL,
	Updated Datetime NOT NULL,
	Role_ID Int NOT NULL,
 Primary Key (Role_user_ID)) ENGINE = MyISAM;

Create table Store (
	Store_ID Int NOT NULL AUTO_INCREMENT,
	Name Varchar(255) NOT NULL,
	Created Datetime NOT NULL,
	Updated Datetime NOT NULL,
 Primary Key (Store_ID)) ENGINE = MyISAM;

Create table Product_store (
	Product_store_ID Int NOT NULL AUTO_INCREMENT,
	Price Float NOT NULL,
	Discount Float,
	Created Datetime NOT NULL,
	Updated Datetime NOT NULL,
	Product_ID Int NOT NULL,
	Store_ID Int NOT NULL,
 Primary Key (Product_store_ID)) ENGINE = MyISAM;

Create table Product (
	Product_ID Int NOT NULL AUTO_INCREMENT,
	Name Varchar(255) NOT NULL,
	Description Text,
	Code Varchar(255) NOT NULL,
	Created Datetime NOT NULL,
	Updated Datetime NOT NULL,
 Primary Key (Product_ID)) ENGINE = MyISAM;

Create table Role_user_store (
	Role_user_store_ID Int NOT NULL AUTO_INCREMENT,
	Created Datetime NOT NULL,
	Updated Datetime NOT NULL,
	Store_ID Int NOT NULL,
	Role_user_ID Int NOT NULL,
 Primary Key (Role_user_store_ID)) ENGINE = MyISAM;


Alter table Role_user add Foreign Key (User_ID) references User (User_ID) on delete  restrict on update  restrict;
Alter table Role_user add Foreign Key (Role_ID) references Role (Role_ID) on delete  restrict on update  restrict;
Alter table Role_user_store add Foreign Key (Role_user_ID) references Role_user (Role_user_ID) on delete  restrict on update  restrict;
Alter table Product_store add Foreign Key (Store_ID) references Store (Store_ID) on delete  restrict on update  restrict;
Alter table Role_user_store add Foreign Key (Store_ID) references Store (Store_ID) on delete  restrict on update  restrict;
Alter table Product_store add Foreign Key (Product_ID) references Product (Product_ID) on delete  restrict on update  restrict;


